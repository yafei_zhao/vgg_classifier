#!/bin/bash

# - This script has been tested with macOS Sierra v10.12.3
# - It assumes Homebrew is available in the system (https://brew.sh/).
# - It assumes GIT is installed (https://sourceforge.net/projects/git-osx-installer/files/)
# - Make sure you have administrator user priviledges

#install basic stuff
brew update
brew install wget
brew install cmake
#brew install python, if not already installed (up to python 2.7.13 is supported)

#caffe dependencies
brew install -vd snappy leveldb gflags glog szip lmdb
brew tap homebrew/science
brew install hdf5 opencv
brew install protobuf@2.5 boost@1.57
brew install openblas
brew install zeromq
brew install git

#link some stuff
brew link glog
brew link lmdb
brew link snappy
brew link --force protobuf@2.5
brew link --force boost@1.57
brew link --force openblas
brew link zeromq

# clone git repo
cd $HOME
git clone https://gitlab.com/vgg/vgg_classifier.git

# setup folders
mkdir $HOME/vgg_classifier/dependencies

# download caffe
wget https://github.com/BVLC/caffe/archive/rc3.zip
unzip rc3.zip -d $HOME/vgg_classifier/dependencies

# download cpp-netlib
wget https://github.com/kencoken/cpp-netlib/archive/0.11-devel.zip
unzip 0.11-devel.zip -d $HOME/vgg_classifier/dependencies

# download liblinear
wget https://github.com/cjlin1/liblinear/archive/v210.zip
unzip v210.zip -d $HOME/vgg_classifier/dependencies

# compile caffe
mv $HOME/vgg_classifier/dependencies/caffe-rc3/ $HOME/vgg_classifier/dependencies/caffe/
cd $HOME/vgg_classifier/dependencies/caffe/
cp Makefile.config.example Makefile.config
sed -i '.sed' 's/# CPU_ONLY/CPU_ONLY/g' Makefile.config
sed -i '.sed' 's/# BLAS_INCLUDE := $(/BLAS_INCLUDE := $(/g' Makefile.config
sed -i '.sed' 's/# BLAS_LIB := $(/BLAS_LIB := $(/g' Makefile.config
make all

# compile cpp-netlib
cd  $HOME/vgg_classifier/dependencies/cpp-netlib-0.11-devel/
mkdir build
cd build
cmake -DOPENSSL_INCLUDE_DIR=/usr/local/opt/openssl/include -DOPENSSL_SSL_LIBRARY=/usr/local/opt/openssl/lib/libssl.dylib ../
make

# compile liblinear
cd $HOME/vgg_classifier/dependencies/liblinear-210/
make lib
ln -s liblinear.so.3 liblinear.so

#if the C++ ZMQ port is not installed, vgg_classifier won't compile.
#do this to obtain it.
#wget https://raw.githubusercontent.com/zeromq/cppzmq/master/zmq.hpp
#cp zmq.hpp /usr/local/include/

# vgg_classifier additional dependencies
pip install protobuf==2.6.1
pip install gevent-zeromq==0.2.5

# compile and install vgg_classifier
cd $HOME/vgg_classifier/
mkdir build
cd build
cmake -DCaffe_DIR=$HOME/vgg_classifier/dependencies/caffe \
      -DCaffe_INCLUDE_DIR="$HOME/vgg_classifier/dependencies/caffe/include;$HOME/vgg_classifier/dependencies/caffe/build/src" \
      -DLiblinear_DIR=$HOME/vgg_classifier/dependencies/liblinear-210/ \
      -Dcppnetlib_DIR=$HOME/vgg_classifier/dependencies/cpp-netlib-0.11-devel/build/ ../
make
make install

#add dylb paths to vgg_classifier dependencies
echo "export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$HOME/vgg_classifier/dependencies/liblinear-210:$HOME/vgg_classifier/dependencies/caffe/build/lib" >> $HOME/.profile
