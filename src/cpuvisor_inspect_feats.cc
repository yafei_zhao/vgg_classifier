#include <glog/logging.h>
#include <gflags/gflags.h>

#include "server/util/io.h"

DEFINE_string(feats_file, "", "Features file");

int main(int argc, char* argv[]) {

  google::InstallFailureSignalHandler();
  google::SetUsageMessage("Inspect binaryproto feature file");
  google::ParseCommandLineFlags(&argc, &argv, true);

  CHECK_NE(FLAGS_feats_file, "");

  std::cout << "\nInspecting " << FLAGS_feats_file << std::endl;

  cv::Mat feats;
  std::vector<std::string> paths;

  CHECK(cpuvisor::readFeatsFromProto(FLAGS_feats_file,
                                     &feats,
                                     &paths));

  std::cout << "paths.size()=" << paths.size() <<std::endl;
  std::cout << "feats.size()=" << feats.size() <<std::endl;

  for (size_t i = 0; i < paths.size(); ++i) {
    std::cout << "\n" << i << ": " << paths[i] << std::endl;
    std::cout << feats.row(i) << std::endl;
  }

  return 0;

}
